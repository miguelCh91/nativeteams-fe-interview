import Vue from "vue";
import Vuex from "vuex";
import noticeboard from "./modules/noticeboard";
import holidays from "./modules/holidays";
import userData from "./modules/userData";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    noticeboard,
    holidays,
    userData,
  },
});
