import axios from "axios";

const state = {
  notices: [],
};

const getters = {
  notices: (state) => state.notices,
};

const actions = {
  async getNotices({ commit }) {
    try {
      const response = await axios.get("http://localhost:8000/notices");
      commit("GET_NOTICES", response.data);
    } catch (error) {
      console.error(error);
    }
  },
  async addNotice({ commit }, notice) {
    try {
      const response = await axios.post(
        "http://localhost:8000/notices",
        notice
      );
      commit("ADD_NOTICE", response.data);
    } catch (error) {
      console.error(error);
    }
  },
  async updateNotice({ commit }, notice) {
    try {
      const response = await axios.put(
        `http://localhost:8000/notices/${notice.id}`,
        notice
      );
      commit("UPDATE_NOTICE", response.data);
    } catch (error) {
      console.error(error);
    }
  },
  async deleteNotice({ commit }, id) {
    try {
      await axios.delete(`http://localhost:8000/notices/${id}`);
      commit("DELETE_NOTICE", id);
    } catch (error) {
      console.error(error);
    }
  },
};

const mutations = {
  GET_NOTICES(state, notices) {
    state.notices = notices;
  },
  ADD_NOTICE(state, notice) {
    state.notices.push(notice);
  },
  UPDATE_NOTICE(state, updatedNotice) {
    const index = state.notices.findIndex(
      (updatedNotice) => updatedNotice.id === updatedNotice.id
    );
    if (index !== -1) {
      state.notices = [
        ...state.notices.slice(0, index),
        updatedNotice,
        ...state.notices.slice(index + 1),
      ];
    }
  },
  DELETE_NOTICE(state, id) {
    state.notices = state.notices.filter((notice) => notice.id !== id);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
