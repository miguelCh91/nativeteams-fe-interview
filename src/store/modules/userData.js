import axios from "axios";

const state = {
  userData: {},
};

const getters = {
  userData: (state) => state.userData,
};

const actions = {
  async getUserData({ commit }) {
    try {
      const response = await axios.get("http://localhost:8000/user");
      commit("GET_USER_DATA", response.data);
    } catch (error) {
      console.error(error);
    }
  },
};

const mutations = {
  GET_USER_DATA(state, data) {
    state.userData = data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
