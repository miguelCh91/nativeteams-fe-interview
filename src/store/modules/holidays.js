import axios from "axios";

const state = {
  holidays: [],
};

const getters = {
  holidays: (state) => state.holidays,
};

const actions = {
  async getHolidays({ commit }) {
    try {
      const response = await axios.get("http://localhost:8000/holidays");
      const holidays = response.data.map((holiday) => ({
        id: holiday.id,
        name: holiday.name,
        start: new Date(holiday.start),
        end: new Date(holiday.end),
        timed: holiday.timed,
        type: holiday.type,
        status: holiday.status,
        note: holiday.note,
        members: holiday.members,
        color: holiday.color,
      }));
      commit("GET_HOLIDAYS", holidays);
    } catch (error) {
      console.error(error);
    }
  },
  async addHoliday({ commit, dispatch }, data) {
    try {
      const response = await axios.post("http://localhost:8000/holidays", data);
      commit("ADD_HOLIDAY", response.data);
      dispatch("getHolidays");
    } catch (error) {
      console.error(error);
    }
  },
  async deleteHoliday({ commit }, id) {
    try {
      await axios.delete(`http://localhost:8000/holidays/${id}`);
      commit("DELETE_HOLIDAY", id);
    } catch (error) {
      console.error(error);
    }
  },
};

const mutations = {
  GET_HOLIDAYS(state, holidays) {
    state.holidays = holidays;
  },
  ADD_HOLIDAY(state, holiday) {
    state.holidays.push(holiday);
  },
  DELETE_HOLIDAY(state, id) {
    state.holidays = state.holidays.filter((holiday) => holiday.id !== id);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
