import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "AbsenceMgt",
    component: () => import("~/views/AbsenceMgt.vue"),
    alias: "/absence-mgt",
    meta: {
      title: "AbsenceMgt",
      layout: "DashboardLayout",
    },
  },
  {
    path: "/noticeboard",
    name: "NoticeBoard",
    component: () => import("~/views/NoticeBoard.vue"),
    alias: "/notice-board",
    meta: {
      title: "NoticeBoard",
      layout: "DashboardLayout",
    },
  },
];

const router = new VueRouter({
  linkActiveClass: "active", // active class for non-exact links.
  linkExactActiveClass: "active", // active class for *exact* links.
  mode: "history",
  routes,
});

export default router;
